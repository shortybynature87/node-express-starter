const express = require('express');

const app = express();

//TODO set security HTTP headers

//TODO parse json request body

//TODO parse urlencoded request body

//TODO sanitize request data

//TODO gzip compression

//TODO enable cors

module.exports = app;