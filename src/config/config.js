const dotenv = require('dotenv');
const path = require('path');
const Joi = require('joi');

dotenv.config({path: path.join(__dirname,'../../.env')});

const envVarsSchema = Joi.object().keys({
    NODE_ENV: Joi.string().valid('production', 'development', 'test', 'local').required(),
    PORT: Joi.number().default(3000),
    MONGODB_URL: Joi.string().required().description('Mongo DB url'),
}).unknown();

const { error, value: envVars } = envVarsSchema.prefs({ errors: { label: 'key'}}).validate(process.env);

if (error) {
    //TODO return error message and delete console log !!!
    console.log(`Config validation error: ${error.message}`);
}

module.exports = {
    env: envVars.NODE_ENV,
    port: envVars.PORT,
}


