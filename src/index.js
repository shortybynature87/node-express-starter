const app = require('./app');
const config = require('./config/config');

let server;

server = app.listen(config.port, () => {
    //TODO implement logger and delete console log !!!
    console.log(`Listening to port ${config.port}`)
});

const exitHandler = () => {
    if (server) {
        server.close(() => {
            //TODO implement logger and delete console log !!!
            console.log('Server closed');
            process.exit(1);
        });
    } else {
        process.exit(1);
    }
}

const unexpectedErrorHandler = (error) => {
    //TODO implement logger and delete console log !!!
    console.log(error);
    exitHandler();
}

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
    //TODO implement logger and delete console log !!!
    console.log('SIGTERM received')
    if (server) server.close();
})